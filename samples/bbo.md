# sample for bbo case

this is one bridge hand from bridge base for 
[Tourney 45988-1610708401- played by Rollo100](https://www.bridgebase.com/myhands/hands.php?tourney=45988-1610708401-&username=Rollo100), it exist as [Movie](https://www.bridgebase.com/tools/handviewer.html?bbo=y&myhand=M-775620181-1610708401) and [lin](https://www.bridgebase.com/myhands/fetchlin.php?id=775620181&when_played=1610708401), so let's see what happens for fourth hand. 

I (for example precisionb) sit in south, the complete auction is 

<pre lang="bridge">
https://www.bridgebase.com/myhands/fetchlin.php?id=775620181&when_played=1610708401
auction
<pre>

Now I see the N/S two hands like

<pre lang="bridge">
deal|card=ns|ur="#45988 EBU&match 4/12"
</pre>

It looks it is quite simple, rush to reach 5♥, so bad for bidding.

And the 4 hands are 

<pre lang="bridge">
deal
</pre>

Bad score and good for opp Rollo100 to have 73.61%