# Introduction

There are several utity tools for writing bridge articles

## Key features (beta release)

* support xinrui url, bbo lin url (bbo soon)
* generate well tuned latex/html bridge layout (card diagram) 
* generate all kinds of ebook (`.pdf`/`.mobi`/`.epub`/`.html`/`.latex`) with related toolchains (`pandoc`,`multimarkdown`)

It contains several subpackage

* xin2pbn
* pbn2html
* mdbridge (mdbridge2latex, mdbrdige2html)

## Install

````
pip install bridge-utils
````

# modules

## xin2pbn

xinrui bridge online url to pbn converter

xinrui url looks like [this](http://www.xinruibridge.com/deallog/DealLog.html?bidlog=P%3BP,1S,X,2S%3BP,P,X,P%3B3C,P,4C,P%3B5C,P,P,P%3B&playlog=W:TH,KH,AH,4H%3BE:TS,KS,AS,6C%3BN:3H,2H,9H,8H%3BS:TD,KD,AD,7D%3BN:3D,9D,2C,2D%3BS:3C,4C,TC,5C%3BN:6D,5D,QC,4D%3BS:7C,KC,AC,8C%3BN:&deal=T96.A62.J975.985%20KQ832.954.T.Q732%20AJ754.T8.K842.K4%20.KQJ73.AQ63.AJT6&vul=NS&dealer=E&contract=5C&declarer=S&wintrick=12&score=620&str=%E7%BE%A4%E7%BB%84IMP%E8%B5%9B%2020201121%20%E7%89%8C%E5%8F%B7%202/8&dealid=984602529&pbnid=344008254), it can be converted to standard pbn files [output.pbn](output.pbn)

### Usage

````
pip install xin2pbn
xin2pbn "url"
xin2pbn "local.html"
````

### example

````
$ xin2pbn "http://www.xinruibridge.com/deallog/DealLog.html?bidlog=P%3BP,1S,X,2S%3BP,P,X,P%3B3C,P,4C,P%3B5C,P,P,P%3B&playlog=W:TH,KH,AH,4H%3BE:TS,KS,AS,6C%3BN:3H,2H,9H,8H%3BS:TD,KD,AD,7D%3BN:3D,9D,2C,2D%3BS:3C,4C,TC,5C%3BN:6D,5D,QC,4D%3BS:7C,KC,AC,8C%3BN:&deal=T96.A62.J975.985%20KQ832.954.T.Q732%20AJ754.T8.K842.K4%20.KQJ73.AQ63.AJT6&vul=NS&dealer=E&contract=5C&declarer=S&wintrick=12&score=620&str=%E7%BE%A4%E7%BB%84IMP%E8%B5%9B%2020201121%20%E7%89%8C%E5%8F%B7%202/8&dealid=984602529&pbnid=344008254"
write to file output.pbn
write to file output.html

$ curl -O https://isoliu.gitlab.io/book2020/web/1-chapter2-B01-zdjs.html
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  5054  100  5054    0     0  11512      0 --:--:-- --:--:-- --:--:-- 11512
$ xin2pbn 1-chapter2-B01-zdjs.html
[b'https://isoliu.gitlab.io/deallog/DealLog.html?bidlog=1D,1S,P;2D,3C,P,P;4S,P,P,X;P,P,P&amp;playlog=E:5D,KD,2D,6D;S:3H,5H,8H,7H;N:8S,TS,AS,3D;S:4C,KC,3C,2C;W:8D,7D,2S,9D;E:8C,JC,AC,5C;W:QD,TH,5S,4D;E:6H,9H,KH,AH;N:&amp;deal=.K5.QJT832.AK976%20KQ874.AT8.76.Q53%20JT652.7642.5.T82%20A93.QJ93.AK94.J4&amp;vul=All&amp;dealer=W&amp;contract=4SX&amp;declarer=N&amp;wintrick=9&amp;score=-200&amp;str=%E9%94%A6%E6%A0%87%E8%B5%9B%20%E7%AC%AC12%E8%BD%AE%20%E7%89%8C%E5%8F%B7%204/12&amp;dealid=653117488&amp;pbnid=127397878', b'https://isoliu.gitlab.io/deallog/DealLog.html?bidlog=1D,1S,P;2D,3C,P,P;4S,P,P,X;P,P,P&amp;playlog=E:5D,AD,2D,6D;S:QH,KH,AH,7H;N:KS,2S,3S,3D;N:4S,TS,AS,8D;S:3H,5H,8H,2H;N:TH,6H,JH,7C;S:9H,6C,7D,4H;S:4C,KC,3C,2C;W:AC,5C,8C,JC;W:QD,QS,TC,4D;N:&amp;deal=.K5.QJT832.AK976%20KQ874.AT8.76.Q53%20JT652.7642.5.T82%20A93.QJ93.AK94.J4&amp;vul=All&amp;dealer=W&amp;contract=4SX&amp;declarer=N&amp;wintrick=10&amp;score=790&amp;str=%E9%94%A6%E6%A0%87%E8%B5%9B%20%E7%AC%AC12%E8%BD%AE%20%E7%89%8C%E5%8F%B7%204/12&amp;dealid=653247876&amp;pbnid=127397878']
write to file ./1-chapter2-B01-zdjs-01.pbn
write to file ./1-chapter2-B01-zdjs-02.pbn

$ cat 1-cha*.pbn > all.pbn
````

Open `output.html` can browse the playing log locally

### snapshot

original source in xinrui UI

![](xinrui.png)

The outputed pbn can be viewed inside bridgecomposer & pbnjview

![](bridgecomposer.png)
![](pbnjview.png)

# Other use cases

## Use case 1: for student newsletter

This is for http://xrgopher.gitee.io/studentbridge

````
zyj-update README.txt
````

Using README.txt, all the materials are combined into `README.md` with xinrui links, then generate HTMLs

````
zyj-convert README.md
````

Then `README.html` and `README.pbn.html` are generated

## Use case 2: mdbridge - Article Samples 

* Ramsey's article for xinrui bridge (Chinese), see [ramsey.md](https://xrgopher.gitlab.io/mdbridge/ramsey.md) and [ramsey.pdf](https://xrgopher.gitlab.io/mdbridge/ramsey.pdf)

You need install [MulitMarkdown](https://fletcherpenney.net/multimarkdown/download/) first

### How to use it

Generally, follow below steps 

* write it in special format with markdown
* use `mdbridge` tool to generate intermediate markdown file
* use `multimarkdown` or `pandoc` to generate related format files
* generate final ebook

See below for latex 

````
$ mdbrdige2latex sample # download sample files ramsey.md, meta.txt
$ mdbridge2latex ramsey.md
ramsey.bridge-tex is created
$ multimarkdown -t latex meta.txt ramsey.bridge-tex -o ramsey.tex
$ xelatex ramsey.tex # use overleaf if u don't have latex env.
````

Then you can edit the file in [overleaf](https://www.overleaf.com) like [ramsey.tex@overleaf.com](https://www.overleaf.com/read/kzwczwjqhxhr)

See below for html

````
$ mdbridge2html ramsey.md
processing ramsey.md -> ramsey.bridge
write to file interesting.pbn
write to file interesting.pbn
$ multimarkdown.exe  -t html ramsey.bridge -o ramsey.html
````

# Usage in detail 

## markdown format

### define the deal from url first

<pre lang="bridge">
http://www.xinruibridge.com/deallog/DealLog.html?bidlog=P,2N,P%3B3C,P,3N,P%3B6N,P,P,P%3B&playlog=E:KD,3D,4D,JD%3BE:2D,5D,7D,AD%3BN:JS,6S,5S,8S%3BN:KS,4S,7S,2S%3BN:3S,TS,AS,8H%3BS:QS,TD,4C,9S%3BS:KH,JH,4H,2H%3BS:AH,TH,9H,3H%3BS:QH,9D,8C,5H%3BS:2C,JC,QC,6C%3BN:KC,9C,6D,5C%3BN:AC,7H,6H,3C%3BN:7C,QD,8D,TC%3B&deal=82.JT8.T974.JT53%20KJ3.94.AJ.AKQ874%20T964.7532.KQ2.96%20AQ75.AKQ6.8653.2&vul=All&dealer=W&contract=6N&declarer=N&wintrick=11&score=-100&str=%E7%BE%A4%E7%BB%84IMP%E8%B5%9B%2020201209%20%E7%89%8C%E5%8F%B7%204/8&dealid=995050099&pbnid=345464272
auction
</pre>

### customize the deal

`deal|cards=NS|ul="<str>"|ll=<str>|ur=<str>`

Two-Hand Diagram

<pre lang="bridge">
deal|cards=NS
</pre>

All-Hands Diagram

<pre lang="bridge">
deal
</pre>

Partial deal

<pre lang="bridge">
deal=.xxxx..xxx&.T4.A.AK87&-&.AKQ6.865.
</pre>

Partial deal with extra information

<pre lang="bridge">
deal=.xxxx..xxx&.94.A.AK87&-&.AKQ6.865.|ll="NS 4/12&EW 0"|ur="match 4/8"
</pre>

# Collaborator

* Ramsey @ xinrui : mainly for latex template and tune the card diagrams

# Reference

* http://www.rpbridge.net/7z69.htm
